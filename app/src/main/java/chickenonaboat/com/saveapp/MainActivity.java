package chickenonaboat.com.saveapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import chickenonaboat.com.saveapp.imageuploader.ImageUploadActivity;
import chickenonaboat.com.saveapp.qrcodereader.QRCodeReaderActivity;
import chickenonaboat.com.saveapp.sales.AddSaleActivity;
import chickenonaboat.com.saveapp.sales.SalesActivity;
import chickenonaboat.com.saveapp.sales.graphs.PieProductActivity;
import chickenonaboat.com.saveapp.sales.graphs.YearSalesGraphActivity;

public class MainActivity extends AppCompatActivity {

    private Button salesButton;
    private Button addsaleButton;
    private Button barChartButton;
    private Button pieChartButton;
    private Button qrCodeReaderButton;
    private Button uploadImageButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        salesButton = findViewById(R.id.main_button_sales);
        addsaleButton = findViewById(R.id.main_button_addsale);
        barChartButton = findViewById(R.id.main_button_salesmonth);
        pieChartButton = findViewById(R.id.main_button_salesproduct);
        qrCodeReaderButton = findViewById(R.id.main_button_qrcodereader);
        uploadImageButton = findViewById(R.id.main_button_imageupload);

        salesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SalesActivity.class);
                //finish();
                startActivity(intent);
            }
        });

        addsaleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddSaleActivity.class);
                //finish();
                startActivity(intent);
            }
        });

        barChartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, YearSalesGraphActivity.class);
                //finish();
                startActivity(intent);
            }
        });

        pieChartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, PieProductActivity.class);
                //finish();
                startActivity(intent);
            }
        });

        qrCodeReaderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, QRCodeReaderActivity.class);
                //finish();
                startActivity(intent);
            }
        });

        uploadImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ImageUploadActivity.class);
                startActivity(intent);
            }
        });

    }
}
