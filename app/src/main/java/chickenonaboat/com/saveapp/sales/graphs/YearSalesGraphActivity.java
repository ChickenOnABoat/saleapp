package chickenonaboat.com.saveapp.sales.graphs;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.utils.EntryXComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import chickenonaboat.com.saveapp.R;
import chickenonaboat.com.saveapp.sales.database.Sale;

public class YearSalesGraphActivity extends AppCompatActivity {

    private BarChart barChart;

    private ChartsViewModel chartsViewModel;

    private Spinner yearsSpinner;

    List<BarEntry> barentries = new ArrayList<BarEntry>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_year_sales_graph);

        barChart = findViewById(R.id.chart);

        chartsViewModel = ViewModelProviders.of(this).get(ChartsViewModel.class);

        yearsSpinner = findViewById(R.id.yearsales_spinner_years);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.years_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        yearsSpinner.setAdapter(adapter);

        yearsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                chartsViewModel.getSalesByYear(yearsSpinner.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        chartsViewModel.getSearchResults().observe(this, new Observer<List<Sale>>() {
            @Override
            public void onChanged(@Nullable List<Sale> sales) {
                if(sales.size() > 0){
                    barentries = getMonthlySalesByYear(sales);
                    Collections.sort(barentries, new EntryXComparator());
                    BarDataSet barDataSet = new BarDataSet(barentries, "Vendas");

                    ArrayList<String> xLabel = new ArrayList<>();
                    xLabel.add("Jan");
                    xLabel.add("Fev");
                    xLabel.add("Mar");
                    xLabel.add("Abr");
                    xLabel.add("Mai");
                    xLabel.add("Jun");
                    xLabel.add("Jul");
                    xLabel.add("Ago");
                    xLabel.add("Set");
                    xLabel.add("Out");
                    xLabel.add("Nov");
                    xLabel.add("Dez");

                    BarData barData = new BarData(barDataSet);
                    barChart.setDrawGridBackground(false);
                    Description description = new Description();
                    description.setText("Vendas por mês no ano de" + yearsSpinner.getSelectedItem().toString());
                    barChart.setDescription(description);
                    XAxis xAxis = barChart.getXAxis();
                    xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

                    xAxis.setDrawGridLines(false);
                    xAxis.setGranularity(1f); // only intervals of 1 day
                    xAxis.setLabelCount(12);

                    barChart.getXAxis().setValueFormatter(new ValueFormatter() {
                        @Override
                        public String getFormattedValue(float value) {
                            ArrayList<String> xLabel = new ArrayList<>();
                            xLabel.add("Jan");
                            xLabel.add("Fev");
                            xLabel.add("Mar");
                            xLabel.add("Abr");
                            xLabel.add("Mai");
                            xLabel.add("Jun");
                            xLabel.add("Jul");
                            xLabel.add("Ago");
                            xLabel.add("Set");
                            xLabel.add("Out");
                            xLabel.add("Nov");
                            xLabel.add("Dez");
                            return xLabel.get(Math.round(value) - 1);
                        }

                        @Override
                        public String getAxisLabel(float value, AxisBase axis) {
                            return super.getAxisLabel(value, axis);
                        }

                        @Override
                        public String getBarLabel(BarEntry barEntry) {
                            return super.getBarLabel(barEntry);
                        }
                    });
                    barChart.setData(barData);
                    barChart.invalidate();
                }else{
                    //BarData emptyBarData = new BarData();
                    barChart.setData(null);
                    barChart.setNoDataText("Sem dados disponíveis para o ano escolhido");
                    barChart.invalidate();
                }
            }
        });



    }



    public List<BarEntry> getMonthlySalesByYear(List<Sale> sales){
        List<BarEntry> entries = new ArrayList<BarEntry>();

        Map<Integer, Integer> duplicates = new HashMap<Integer, Integer>();
        if (sales.size() == 0){
            return null;
        }
        for(int i = 0; i<sales.size(); i++){

                if (duplicates.containsKey(sales.get(i).getMonth_id())) {
                    duplicates.put(sales.get(i).getMonth_id(), duplicates.get(sales.get(i).getMonth_id()) + 1);
                } else {
                    duplicates.put(sales.get(i).getMonth_id(), 1);
                }

        }
        for(int i = 1; i < 13; i++){
            if(duplicates.containsKey(i)){
                entries.add(new BarEntry(i, duplicates.get(i)));
            }else {
                entries.add(new BarEntry(i, 0));
            }
        }
        return entries;
    }
}
