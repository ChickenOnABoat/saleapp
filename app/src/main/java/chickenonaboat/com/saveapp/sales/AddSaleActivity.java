package chickenonaboat.com.saveapp.sales;

import android.app.DatePickerDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import chickenonaboat.com.saveapp.MainActivity;
import chickenonaboat.com.saveapp.R;
import chickenonaboat.com.saveapp.sales.database.Sale;

public class AddSaleActivity extends AppCompatActivity {

    private EditText editTextOrderNumber;
    private EditText editTextQuantity;
    private EditText editTextPrice;
    private EditText editTextSaleDate;
    private Spinner spinnerProduct;
    private Spinner spinnerCountry;

    private Button addSaleButton;

    private DatePickerDialog picker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_sale);

        final SaleViewModel saleViewModel = ViewModelProviders.of(this).get(SaleViewModel.class);

        editTextOrderNumber = findViewById(R.id.addsale_edittext_ordernumber);
        editTextQuantity = findViewById(R.id.addsale_edittext_quantity);
        editTextPrice = findViewById(R.id.addsale_edittext_price);
        editTextSaleDate = findViewById(R.id.addsale_edittext_datesale);

        spinnerProduct = findViewById(R.id.addsale_spinner_productline);
        spinnerCountry = findViewById(R.id.addsale_spinner_country);

        addSaleButton = findViewById(R.id.addsale_button_addsale);

        editTextSaleDate.setFocusable(false);
        editTextSaleDate.setClickable(true);

        editTextSaleDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                picker = new DatePickerDialog(AddSaleActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                editTextSaleDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, year, month, day);
                picker.show();
            }
        });

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.products_array, android.R.layout.simple_spinner_item);
            // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinnerProduct.setAdapter(adapter);

        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,
                R.array.country_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinnerCountry.setAdapter(adapter2);

        addSaleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editTextOrderNumber.length() == 0 || editTextPrice.length() == 0 || editTextQuantity.length() == 0){
                    Toast.makeText(AddSaleActivity.this, "Por favor, preencher todos os campos.", Toast.LENGTH_SHORT).show();
                }else{
                    Sale sale = new Sale();
                    sale.setOrderNumber(Integer.parseInt(editTextOrderNumber.getText().toString()));
                    sale.setPriceEach(Float.parseFloat(editTextPrice.getText().toString()));
                    sale.setQuantityOrdered(Integer.parseInt(editTextQuantity.getText().toString()));
                    sale.setOrderDate(editTextSaleDate.getText().toString());
                    String[] parts = editTextSaleDate.getText().toString().split("/");
                    sale.setMonth_id(Integer.parseInt(parts[1]));
                    sale.setYear_id(Integer.parseInt(parts[2]));
                    sale.setLastUpdated(getCurrentDate());
                    sale.setCountry(spinnerCountry.getSelectedItem().toString());
                    sale.setProductLine(spinnerProduct.getSelectedItem().toString());
                    sale.setPending(true);
                    saleViewModel.addSale(sale);
                    Toast.makeText(AddSaleActivity.this, "Venda Adicionada com Sucesso!", Toast.LENGTH_LONG);
                    finish();
                }
            }
        });

    }

    public static String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date today = Calendar.getInstance().getTime();
        return dateFormat.format(today);
    }
}
