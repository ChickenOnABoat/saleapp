package chickenonaboat.com.saveapp.sales;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import java.util.List;

import chickenonaboat.com.saveapp.appdatabase.AppRepository;
import chickenonaboat.com.saveapp.sales.database.Sale;

public class SaleViewModel extends AndroidViewModel {

    private AppRepository appRepository;
    private LiveData<List<Sale>> allSales;
    private LiveData<List<Sale>> searchResults;

    public SaleViewModel(@NonNull Application application) {
        super(application);
        appRepository = AppRepository.getInstance(application);
        allSales = appRepository.getAllSales();
        searchResults = appRepository.getSearchResults();
    }


    public void addSale(Sale sale){
        appRepository.insertSale(sale);
    }

    LiveData<List<Sale>> getAllSales() {
        return allSales;
    }



}
