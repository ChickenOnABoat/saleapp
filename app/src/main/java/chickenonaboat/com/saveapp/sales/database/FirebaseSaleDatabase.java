package chickenonaboat.com.saveapp.sales.database;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import chickenonaboat.com.saveapp.imageuploader.Imageup;

public class FirebaseSaleDatabase {

    private static volatile FirebaseSaleDatabase instance;

    private FirebaseDatabase firebaseDatabase;
    private FirebaseStorage firebaseStorage;
    private Context context;

    private MutableLiveData<List<Sale>> salesRemote = new MutableLiveData<>();
    private MutableLiveData<HashMap<String, Sale>> salesRemoteMap = new MutableLiveData<>();

    // --- INSTANCE ---
    public static FirebaseSaleDatabase getInstance(Context context) {
        if (instance == null) {
            synchronized (FirebaseSaleDatabase.class) {
                if (instance == null) {
                    instance = new FirebaseSaleDatabase(context);

                }
            }
        }
        return instance;
    }

    public void sendToRemote( Sale sale){
        DatabaseReference saleRef = FirebaseDatabase.getInstance().getReference().child("sales").child(String.valueOf(sale.getOrderNumber()));

        saleRef.child("quantityOrdered").setValue(sale.getQuantityOrdered());
        saleRef.child("priceEach").setValue(sale.getPriceEach());
        saleRef.child("orderDate").setValue(sale.getOrderDate());
        saleRef.child("month_id").setValue(sale.getMonth_id());
        saleRef.child("year_id").setValue(sale.getYear_id());
        saleRef.child("productLine").setValue(sale.getProductLine());
        saleRef.child("country").setValue(sale.getCountry());
        saleRef.child("lastUpdated").setValue(sale.getLastUpdated());

    }

    public void sendImageToRemote(Imageup img){
        StorageReference storageRef = firebaseStorage.getReference();
        DatabaseReference saleRef = FirebaseDatabase.getInstance().getReference().child("images").child(String.valueOf(img.getId()));

        saleRef.child("lastUpdated").setValue(getCurrentDate());
        ContextWrapper cw = new ContextWrapper(context);
        // path to /data/data/yourapp/app_data/imageDir
        Bitmap bitmap = img.getImage();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = storageRef.child("images").child(String.valueOf(img.getId())).putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
                Log.d("FirebaseSaleDatabase", "Upload deu errado");
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
                Log.d("FirebaseSaleDatabase", "Upload deu certo");
            }
        });

    }

    public LiveData<List<Sale>> getSalesRemote(){
        return salesRemote;
    }

    public LiveData<HashMap<String, Sale>> getSalesRemoteMap(){
        return salesRemoteMap;
    }

    public FirebaseSaleDatabase(Context context){
        this.context = context;
        firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseStorage = FirebaseStorage.getInstance();
        firebaseDatabase.getReference().child("sales").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<Sale> sales = new ArrayList<>();
                HashMap<String, Sale> salesMap = new HashMap<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Sale sale = snapshot.getValue(Sale.class);
                    sale.setOrderNumber(Integer.parseInt(snapshot.getKey()));
                    sales.add(sale);
                    salesMap.put(String.valueOf(sale.getOrderNumber()), sale);
                }
                salesRemote.setValue(sales);
                salesRemoteMap.setValue(salesMap);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public static String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date today = Calendar.getInstance().getTime();
        return dateFormat.format(today);
    }


}
