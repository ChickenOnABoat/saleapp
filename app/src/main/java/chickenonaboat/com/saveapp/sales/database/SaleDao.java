package chickenonaboat.com.saveapp.sales.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface SaleDao {

    @Query("SELECT * FROM sale WHERE orderNumber = :ordernumber")
    List<Sale> getItem(String ordernumber);

    @Query("SELECT * FROM sale")
    LiveData<List<Sale>> getItems();

    @Query("SELECT * FROM sale")
    List<Sale> getItemsList();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insertItem(Sale sale);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    int updateItem(Sale sale);

    @Query("DELETE FROM sale WHERE orderNumber = :ordernumber")
    int deleteItem(String ordernumber);

    @Query("SELECT * FROM sale WHERE year_id = :year_id")
    List<Sale> getItemsYearList(int year_id);

    @Query("SELECT * FROM sale WHERE pending = :pending")
    LiveData<List<Sale>> getPendingItemList(Boolean pending);

}
