package chickenonaboat.com.saveapp.sales.graphs;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import chickenonaboat.com.saveapp.R;
import chickenonaboat.com.saveapp.sales.database.Sale;

public class PieProductActivity extends AppCompatActivity {

    private PieChart pieChart;

    private ChartsViewModel chartsViewModel;

    List<PieEntry> pieentries = new ArrayList<PieEntry>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pie_product);

        pieChart = findViewById(R.id.piechart);

        chartsViewModel = ViewModelProviders.of(this).get(ChartsViewModel.class);

        chartsViewModel.getAllSales().observe(this, new Observer<List<Sale>>() {
            @Override
            public void onChanged(@Nullable List<Sale> sales) {
                if(sales.size() > 0){
                    pieentries = getSalesByProduct(sales);

                    PieDataSet set = new PieDataSet(pieentries, "Election Results");
                    ArrayList<Integer> colors = new ArrayList<>();

                    for (int c : ColorTemplate.VORDIPLOM_COLORS)
                        colors.add(c);

                    for (int c : ColorTemplate.JOYFUL_COLORS)
                        colors.add(c);

                    for (int c : ColorTemplate.COLORFUL_COLORS)
                        colors.add(c);

                    for (int c : ColorTemplate.LIBERTY_COLORS)
                        colors.add(c);

                    for (int c : ColorTemplate.PASTEL_COLORS)
                        colors.add(c);

                    colors.add(ColorTemplate.getHoloBlue());

                    set.setColors(colors);
                    PieData data = new PieData(set);

                    data.setValueFormatter(new PercentFormatter(pieChart));
                    data.setValueTextSize(11f);
                    data.setValueTextColor(Color.BLACK);
                    pieChart.setUsePercentValues(true);
                    pieChart.setData(data);
                    pieChart.invalidate(); // refresh
                }
            }
        });
    }

    public List<PieEntry> getSalesByProduct(List<Sale> sales){
        List<PieEntry> entries = new ArrayList<PieEntry>();

        Map<String, Integer> duplicates = new HashMap<String, Integer>();
        if (sales.size() == 0){
            return null;
        }


        for(int i = 0; i<sales.size(); i++){

            if (duplicates.containsKey(sales.get(i).getProductLine())) {
                duplicates.put(sales.get(i).getProductLine(), duplicates.get(sales.get(i).getProductLine()) + 1);
            } else {
                duplicates.put(sales.get(i).getProductLine(), 1);
            }

        }

        for(Map.Entry<String, Integer> entry : duplicates.entrySet()) {
            String key = entry.getKey();
            Integer value = entry.getValue();
            float percent = (float) (value)/sales.size();
            entries.add(new PieEntry(percent, key));
        }

        return entries;

    }
}
