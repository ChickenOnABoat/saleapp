package chickenonaboat.com.saveapp.sales;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.diegodobelo.expandingview.ExpandingItem;
import com.diegodobelo.expandingview.ExpandingList;
import com.google.firebase.FirebaseApp;

import java.util.List;

import chickenonaboat.com.saveapp.R;
import chickenonaboat.com.saveapp.sales.database.Sale;

public class SalesActivity extends AppCompatActivity {
    private final String TAG = "SalesActivity";

    private ExpandingList expandingList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales);
        FirebaseApp.initializeApp(getApplicationContext());
        SaleViewModel saleViewModel = ViewModelProviders.of(this).get(SaleViewModel.class);

         expandingList = (ExpandingList) findViewById(R.id.expanding_list_main);

        saleViewModel.getAllSales().observe(this, new Observer<List<Sale>>() {
            @Override
            public void onChanged(@Nullable List<Sale> sales) {
                expandingList.removeAllViews();
                for(int i = 0; i < sales.size(); i++){
                    Log.d(TAG, sales.get(i).getProductLine());
                    Sale sale = sales.get(i);
                    ExpandingItem item = expandingList.createNewItem(R.layout.sale_expanding_layout);

                    TextView ordernumber = item.findViewById(R.id.expitem_textview_ordernumber);
                    TextView product = item.findViewById(R.id.expitem_textview_product);

                    ordernumber.setText(String.valueOf(sale.getOrderNumber()));
                    product.setText(sale.getProductLine());

                    item.createSubItems(1);

                    View subItemZero = item.getSubItemView(0);

                    TextView quantity = subItemZero.findViewById(R.id.subextitem_textview_quantity);
                    TextView price = subItemZero.findViewById(R.id.subextitem_textview_price);
                    TextView orderdate = subItemZero.findViewById(R.id.subextitem_textview_orderdate);
                    TextView country = subItemZero.findViewById(R.id.subextitem_textview_country);


                    quantity.setText(String.valueOf(sale.getQuantityOrdered()));
                    price.setText(String.valueOf(sale.getPriceEach()));
                    orderdate.setText(sale.getOrderDate());
                    country.setText(sale.getCountry());

                }
            }
        });

    }
}
