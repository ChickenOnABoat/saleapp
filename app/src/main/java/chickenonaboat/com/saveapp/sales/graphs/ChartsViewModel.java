package chickenonaboat.com.saveapp.sales.graphs;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import chickenonaboat.com.saveapp.appdatabase.AppRepository;
import chickenonaboat.com.saveapp.sales.database.Sale;

public class ChartsViewModel extends AndroidViewModel {

    private AppRepository appRepository;
    private LiveData<List<Sale>> allSales;
    private List<Sale> currentSalesList;

    private LiveData<List<Sale>> queryResults;

    private MutableLiveData<List<BarEntry>> graphData;

    private LiveData<List<Sale>> searchResults;

    public ChartsViewModel(@NonNull Application application) {
        super(application);
        appRepository = AppRepository.getInstance(application);
        allSales = appRepository.getAllSales();
        searchResults = appRepository.getSearchResults();
    }

    LiveData<List<Sale>> getAllSales() {
        return allSales;
    }

    LiveData<List<Sale>> getSearchResults(){
        return searchResults;
    }

    public void getSalesByYear(String year){
        appRepository.getYearSales(Integer.parseInt(year));
    }

    public static void countFrequencies(ArrayList<Sale> list)
    {
        // hashmap to store the frequency of element
        Map<Sale, Integer> hm = new HashMap<Sale, Integer>();

        for (Sale i : list) {
            Integer j = hm.get(i);
            hm.put(i, (j == null) ? 1 : j + 1);
        }

        // displaying the occurrence of elements in the arraylist
        for (Map.Entry<Sale, Integer> val : hm.entrySet()) {
            System.out.println("Element " + val.getKey() + " "
                    + "occurs"
                    + ": " + val.getValue() + " times");
        }
    }

}
