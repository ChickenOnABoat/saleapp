package chickenonaboat.com.saveapp.sales.database;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "sale")
public class Sale {

    public Sale(){
        this.pending = false;
    }

    @PrimaryKey
    private int orderNumber;

    private int quantityOrdered;
    private float priceEach;
    private String orderDate;
    private int month_id;
    private int year_id;
    private String productLine;
    private String country;
    private Boolean pending;
    private String lastUpdated;




    public int getOrderNumber() {
        return orderNumber;
    }

    public int getQuantityOrdered() {
        return quantityOrdered;
    }

    public float getPriceEach() {
        return priceEach;
    }


    public String getOrderDate() {
        return orderDate;
    }

    public int getMonth_id() {
        return month_id;
    }

    public int getYear_id() {
        return year_id;
    }

    public String getProductLine() {
        return productLine;
    }

    public String getCountry() {
        return country;
    }

    public Boolean getPending() {
        return pending;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public void setQuantityOrdered(int quantityOrdered) {
        this.quantityOrdered = quantityOrdered;
    }

    public void setPriceEach(float priceEach) {
        this.priceEach = priceEach;
    }


    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public void setMonth_id(int month_id) {
        this.month_id = month_id;
    }

    public void setYear_id(int year_id) {
        this.year_id = year_id;
    }

    public void setProductLine(String productLine) {
        this.productLine = productLine;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setPending(Boolean pending) {
        this.pending = pending;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
}
