package chickenonaboat.com.saveapp.appdatabase;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.ContentValues;
import android.content.Context;
import android.support.annotation.NonNull;

import chickenonaboat.com.saveapp.imageuploader.Imageup;
import chickenonaboat.com.saveapp.imageuploader.ImageDao;
import chickenonaboat.com.saveapp.sales.database.Sale;
import chickenonaboat.com.saveapp.sales.database.SaleDao;

@Database(entities = {Sale.class, Imageup.class}, version = 3, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    private final String TAG = "AppDatabase";

    // --- SINGLETON ---
    private static volatile AppDatabase instance;

    public abstract SaleDao saleDao();
    public abstract ImageDao imageDao();

    // --- INSTANCE ---
    public static AppDatabase getInstance(Context context) {
        if (instance == null) {
            synchronized (AppDatabase.class) {
                if (instance == null) {
                    instance = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, "MyDatabase.db")
                            //.addCallback(prepopulateDatabase())
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return instance;
    }

    /*public void setupListeners(){
        FirebaseDatabase.getInstance().getReference().child("sales").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //Check if local database is empty, if yes download all data and store locally
                if(allSales.getValue().size() == 0 ){
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Sale sale = snapshot.getValue(Sale.class);
                        sale.setOrderNumber(Integer.parseInt(snapshot.getKey()));
                        sale.setLastUpdated(getCurrentDate());
                        Log.d(TAG, String.valueOf(sale.getOrderNumber()));
                        insertSale(sale);
                    }
                }else{
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Sale localSale = null;

                        try{
                            for(int i = 0; i < allSales.getValue().size(); i++){
                                String sn = String.valueOf(allSales.getValue().get(i).getOrderNumber());
                                String onr = snapshot.getKey();
                                if(sn.equals(onr)){
                                    localSale = allSales.getValue().get(i);
                                    break;
                                }
                            }
                            if(localSale == null){
                                Sale sale = snapshot.getValue(Sale.class);
                                sale.setOrderNumber(Integer.parseInt(snapshot.getKey()));
                                sale.setLastUpdated(getCurrentDate());
                                Log.d(TAG, String.valueOf(sale.getOrderNumber()) + " Date: " + sale.getLastUpdated());
                                insertSale(sale);
                            }else{
                                String dtupdated = snapshot.child("lastUpdated").getValue().toString();
                                SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm");
                                formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
                                Date remoteSaleUpdated = formatter.parse(dtupdated);

                                Date localSaleUpdated = formatter.parse(localSale.getLastUpdated());
                                //allSales.getValue().get;

                                if(remoteSaleUpdated.after(localSaleUpdated)){
                                    Sale sale = snapshot.getValue(Sale.class);
                                    sale.setOrderNumber(Integer.parseInt(snapshot.getKey()));
                                    sale.setLastUpdated(getCurrentDate());
                                    Log.d(TAG, String.valueOf(sale.getOrderNumber()) + " Date: " + sale.getLastUpdated());
                                    updateSale(sale);
                                    //FirebaseDatabase.getInstance().getReference().child("lastUpdated").setValue(dateFormat.format(today));
                                }
                            }

                        }catch (Exception e){
                            Log.d(TAG, e.getMessage());
                            e.printStackTrace();
                        }


                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }*/

    private static Callback prepopulateDatabase(){
        return new Callback() {

            @Override
            public void onCreate(@NonNull SupportSQLiteDatabase db) {

                super.onCreate(db);

                ContentValues contentValues = new ContentValues();

                contentValues.put("orderNumber", "000000");
                contentValues.put("quantityOrdered", 1);
                contentValues.put("priceEach", 1);
                contentValues.put("orderDate", "5/7/2003 0:00");
                contentValues.put("month_id", "7");
                contentValues.put("year_id", "2003");
                contentValues.put("productLine", "Motorcycles");
                contentValues.put("country", "USA");
                contentValues.put("pending", false);


                db.insert("Sale", OnConflictStrategy.REPLACE, contentValues);
            }
        };
    }
}
