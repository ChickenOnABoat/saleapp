package chickenonaboat.com.saveapp.appdatabase;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.AsyncTask;
import android.support.annotation.Nullable;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;


import chickenonaboat.com.saveapp.imageuploader.ImageDao;
import chickenonaboat.com.saveapp.imageuploader.Imageup;
import chickenonaboat.com.saveapp.sales.database.FirebaseSaleDatabase;
import chickenonaboat.com.saveapp.sales.database.Sale;
import chickenonaboat.com.saveapp.sales.database.SaleDao;

public class AppRepository {

    private final String TAG = "AppRepository";

    private MutableLiveData<List<Sale>> searchResults =
            new MutableLiveData<>();

    private LiveData<List<Sale>> allSales;
    private LiveData<HashMap<String, Sale>> allSalesMap;
    private LiveData<List<Sale>> allPendingSales;
    private LiveData<List<Imageup>> allPendingImages;


    private LiveData<List<Imageup>> allImages;

    private LiveData<HashMap<String, Sale>> allRemoteSales;


    private static volatile AppRepository instance;

    private SaleDao saleDao;
    private ImageDao imageDao;

    private FirebaseSaleDatabase firebaseSaleDatabase;
    private AppDatabase db;

    private static Context context;

    public static AppRepository getInstance(Context context) {
        if (instance == null) {
            synchronized (AppRepository.class) {
                if (instance == null) {
                    instance = new AppRepository(context);
                }
            }
        }
        return instance;
    }

    public AppRepository(Context application) {
        context = application;
        firebaseSaleDatabase = FirebaseSaleDatabase.getInstance(application);
        db = AppDatabase.getInstance(application);
        saleDao = db.saleDao();
        imageDao = db.imageDao();

        allSales = saleDao.getItems();
        allImages = imageDao.getItems();

        allPendingSales = saleDao.getPendingItemList(true);
        allPendingImages = imageDao.getPendingItemList(true);

        allRemoteSales = firebaseSaleDatabase.getSalesRemoteMap();

        allPendingSales.observeForever(new Observer<List<Sale>>() {
            @Override
            public void onChanged(@Nullable List<Sale> sales) {
                for(int i = 0; i < sales.size(); i++){
                    firebaseSaleDatabase.sendToRemote(sales.get(i));
                    Sale removePending = sales.get(i);
                    removePending.setPending(false);
                    updateSale(removePending);
                }
            }
        });

        allPendingImages.observeForever(new Observer<List<Imageup>>() {
            @Override
            public void onChanged(@Nullable List<Imageup> imageups) {
                for(int i = 0; i <imageups.size(); i++){
                    Imageup aux = imageups.get(i);
                    aux.setImage(loadImageFromStorage(String.valueOf(aux.getId()), context));
                    try{
                        firebaseSaleDatabase.sendImageToRemote(aux);
                        aux.setPending(false);
                        updateImage(aux);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });



        allSales.observeForever(new Observer<List<Sale>>() {
            @Override
            public void onChanged(@Nullable List<Sale> sales) {
                if(sales != null){
                    allRemoteSales.observeForever(new Observer<HashMap<String, Sale>>() {
                        @Override
                        public void onChanged(@Nullable HashMap<String, Sale> stringSaleHashMap) {

                            List<Sale> localSales = allSales.getValue();
                            if(localSales != null){
                                HashMap<String, Sale> localSalesMap = new HashMap<>();
                                for(int i = 0; i < localSales.size(); i++){
                                    localSalesMap.put(String.valueOf(localSales.get(i).getOrderNumber()), localSales.get(i));
                                }

                                for(Map.Entry<String, Sale> entry : stringSaleHashMap.entrySet()) {
                                    String key = entry.getKey();
                                    Sale remoteSale = entry.getValue();

                                    if(!localSalesMap.containsKey(key)){
                                        //Insert on Local Database
                                        insertSale(remoteSale);
                                    }else{
                                        //Compare and check if the remoteOne is newer
                                        //Create Date formatter
                                        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm");
                                        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
                                        try{
                                            Date remoteSaleUpdated = formatter.parse(remoteSale.getLastUpdated());
                                            Date localSaleUpdated = formatter.parse(localSalesMap.get(key).getLastUpdated());
                                            //If remoteversion is newer, update local
                                            if(remoteSaleUpdated.after(localSaleUpdated)){
                                                //Update local
                                                updateSale(remoteSale);
                                            }
                                            //Date remoteSaleUpdated = formatter.parse(dtupdated)
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                        }
                    });
                }
            }
        });


    }

    private Bitmap loadImageFromStorage(String name, Context context){

        try {
            ContextWrapper cw = new ContextWrapper(context);
            // path to /data/data/yourapp/app_data/imageDir
            String filename = name.concat(".jpeg");
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
            File f=new File(directory, filename);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            //ImageView img=(ImageView)findViewById(R.id.imgPicker);
            //img.setImageBitmap(b);
            return b;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
            return  null;
        }

    }

    public static String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date today = Calendar.getInstance().getTime();
        return dateFormat.format(today);
    }

    public LiveData<List<Sale>> getAllSales() {
        return allSales;
    }

    public LiveData<List<Imageup>> getAllImages() {
        return allImages;
    }

    public List<Sale> getAllSalesList(){
        return allSales.getValue();
    }

    public LiveData<List<Sale>> getSearchResults() {
        return searchResults;
    }

    private void asyncFinished(List<Sale> results) {
        searchResults.setValue(results);
    }

    public void insertSale(Sale newSale) {
        InsertAsyncTask task = new InsertAsyncTask(saleDao);
        task.execute(newSale);
    }

    public void insertImage(Imageup imageup){
        InsertImageAsyncTask task = new InsertImageAsyncTask(imageDao);
        task.execute(imageup);
    }

    public void updateSale(Sale newSale) {
        UpdateAsyncTask task = new UpdateAsyncTask(saleDao);
        task.execute(newSale);
    }

    public void updateImage(Imageup newimg) {
        UpdateImageAsyncTask task = new UpdateImageAsyncTask(imageDao);
        task.execute(newimg);
    }

    public void deleteSale(String name) {
        DeleteAsyncTask task = new DeleteAsyncTask(saleDao);
        task.execute(name);
    }

    public void findSale(String name) {
        QueryAsyncTask task = new QueryAsyncTask(saleDao);
        task.delegate = this;
        task.execute(name);
    }

    public void getYearSales(Integer year){
        QueryYearAsyncTask task = new QueryYearAsyncTask(saleDao);
        task.delegate = this;
        task.execute(year);
    }


    private static class QueryYearAsyncTask extends
            AsyncTask<Integer, Void, List<Sale>> {

        private SaleDao asyncTaskDao;
        private AppRepository delegate = null;

        QueryYearAsyncTask(SaleDao dao) {
            asyncTaskDao = dao;
        }


        @Override
        protected List<Sale> doInBackground(Integer... params) {
            return asyncTaskDao.getItemsYearList(params[0]);
        }

        @Override
        protected void onPostExecute(List<Sale> result) {
            delegate.asyncFinished(result);
        }
    }


    private static class QueryAsyncTask extends
            AsyncTask<String, Void, List<Sale>> {

        private SaleDao asyncTaskDao;
        private AppRepository delegate = null;

        QueryAsyncTask(SaleDao dao) {
            asyncTaskDao = dao;
        }


        @Override
        protected List<Sale> doInBackground(String... params) {
            return asyncTaskDao.getItem(params[0]);
        }

        @Override
        protected void onPostExecute(List<Sale> result) {
            delegate.asyncFinished(result);
        }
    }


    private static class InsertAsyncTask extends AsyncTask<Sale, Void, Void> {

        private SaleDao asyncTaskDao;

        InsertAsyncTask(SaleDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Sale... params) {
            asyncTaskDao.insertItem(params[0]);
            return null;
        }
    }

    private static class UpdateAsyncTask extends AsyncTask<Sale, Void, Void> {

        private SaleDao asyncTaskDao;

        UpdateAsyncTask(SaleDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Sale... params) {
            asyncTaskDao.updateItem(params[0]);
            return null;
        }
    }

    private static class UpdateImageAsyncTask extends AsyncTask<Imageup, Void, Void> {

        private ImageDao asyncTaskDao;

        UpdateImageAsyncTask(ImageDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Imageup... params) {
            asyncTaskDao.updateItem(params[0]);
            return null;
        }
    }

    private static class DeleteAsyncTask extends AsyncTask<String, Void, Void> {

        private SaleDao asyncTaskDao;

        DeleteAsyncTask(SaleDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final String... params) {
            asyncTaskDao.deleteItem(params[0]);
            return null;
        }
    }

    private static class InsertImageAsyncTask extends AsyncTask<Imageup, Void, Imageup> {

        private ImageDao asyncTaskDao;

        InsertImageAsyncTask(ImageDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Imageup doInBackground(final Imageup... params) {
            long id = asyncTaskDao.insertItem(params[0]);
            Imageup img = params[0];
            img.setId((int) id);
            return img;
        }

        @Override
        protected void onPostExecute(Imageup img) {
            super.onPostExecute(img);
            saveToInternalStorage(img.getImage(), String.valueOf(img.getId()), context);
        }
    }

    private static String saveToInternalStorage(Bitmap bitmapImage, String id, Context context){
        ContextWrapper cw = new ContextWrapper(context);
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        String filename = id.concat(".jpeg");
        File mypath=new File(directory,filename);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }

}
