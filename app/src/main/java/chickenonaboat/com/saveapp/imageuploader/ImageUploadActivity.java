package chickenonaboat.com.saveapp.imageuploader;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import chickenonaboat.com.saveapp.R;
import chickenonaboat.com.saveapp.imageuploader.utils.ImageListAdapter;

public class ImageUploadActivity extends AppCompatActivity {

    private final static int REQUEST_GET_SINGLE_FILE = 1;

    private Button addImageButton;

    private ImageViewModel imageViewModel;

    private ImageListAdapter imageListAdapter;

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_upload);

        imageViewModel = ViewModelProviders.of(ImageUploadActivity.this).get(ImageViewModel.class);

        addImageButton = findViewById(R.id.imageupload_button_addimage);

        recyclerView = findViewById(R.id.imageupload_recyclerView);

        imageListAdapter = new ImageListAdapter(new ArrayList<Imageup>());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(imageListAdapter);

        imageViewModel.getAllImages().observe(this, new Observer<List<Imageup>>() {
            @Override
            public void onChanged(@Nullable List<Imageup> imageups) {
                if(imageups != null){
                    ArrayList<Imageup> imgs = new ArrayList<>();
                    for(int i = 0; i < imageups.size(); i++){
                        Imageup aux = imageups.get(i);
                        aux.setImage(loadImageFromStorage(String.valueOf(aux.getId()), getApplicationContext()));
                        imgs.add(aux);
                    }
                    imageListAdapter = new ImageListAdapter(imgs);
                    recyclerView.setAdapter(imageListAdapter);
                }
            }
        });

        addImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Escolha a Imagem"),REQUEST_GET_SINGLE_FILE);
            }
        });

    }

    public void initialize(){

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK) {
                if (requestCode == REQUEST_GET_SINGLE_FILE ) {
                    Uri selectedImageUri = data.getData();
                    // Get the path from the Uri
                    final String path = selectedImageUri.getPath();
                    if (path != null) {
                        /*File f = new File(path);
                        selectedImageUri = Uri.fromFile(f);*/
                        //Bitmap bitmap = BitmapFactory.decodeFile(path);
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
                        //insertImage
                        Imageup img = new Imageup();
                        img.setImage(bitmap);
                        img.setLastUpdated(getCurrentDate());
                        imageViewModel.addImage(img);
                    }
                    // Set the image in ImageView
                    //ImageView((ImageView) findViewById(R.id.imgView)).setImageURI(selectedImageUri);
                }
            }
        } catch (Exception e) {
            Log.d("FileSelectorActivity", "File select error", e);
        }
    }

    public static String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date today = Calendar.getInstance().getTime();
        return dateFormat.format(today);
    }

    private Bitmap loadImageFromStorage(String name, Context context){

        try {
            ContextWrapper cw = new ContextWrapper(context);
            // path to /data/data/yourapp/app_data/imageDir
            String filename = name.concat(".jpeg");
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
            File f=new File(directory, filename);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            //ImageView img=(ImageView)findViewById(R.id.imgPicker);
            //img.setImageBitmap(b);
            return b;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
            return  null;
        }

    }

    public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }


}
