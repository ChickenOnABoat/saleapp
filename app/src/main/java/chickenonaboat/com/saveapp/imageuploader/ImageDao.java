package chickenonaboat.com.saveapp.imageuploader;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface ImageDao {

    @Query("SELECT * FROM image WHERE id = :ordernumber")
    List<Imageup> getItem(String ordernumber);

    @Query("SELECT * FROM image")
    LiveData<List<Imageup>> getItems();

    @Query("SELECT * FROM image")
    List<Imageup> getItemsList();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insertItem(Imageup sale);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    int updateItem(Imageup sale);

    @Query("DELETE FROM image WHERE id = :ordernumber")
    int deleteItem(String ordernumber);


    @Query("SELECT * FROM image WHERE pending = :pending")
    LiveData<List<Imageup>> getPendingItemList(Boolean pending);
}
