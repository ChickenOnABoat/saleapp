package chickenonaboat.com.saveapp.imageuploader;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import java.util.List;

import chickenonaboat.com.saveapp.appdatabase.AppRepository;
import chickenonaboat.com.saveapp.sales.database.Sale;

public class ImageViewModel extends AndroidViewModel {

    private AppRepository appRepository;
    private LiveData<List<Imageup>> allImages;


    public ImageViewModel(@NonNull Application application) {
        super(application);
        appRepository = AppRepository.getInstance(application);
        allImages = appRepository.getAllImages();

    }

    public void addImage(Imageup img){
        appRepository.insertImage(img);
    }

    LiveData<List<Imageup>> getAllImages() {
        return allImages;
    }

}
